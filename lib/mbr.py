#!/usr/bin/python3
# coding: utf8

"""
Written by:
	- Ninon MAITRE
	- Edouard SCHWEISGUTH
	- Eric LO
	- Lucas MARCHAL
	- Pierre LIVET
"""


""""
MBR Class

	Boot code - 		Maybe 440 bytes but can be up to 446 max
	Disk's signature - 	On 4 bytes
	Null part -			On 2 bytes

	# Table of partitions entries, usually 4 entries of 16 bytes
	Partition's status 		-	On 1 byte
	Partition's 1st CHS 	-	On 3 bytes
	Partition's type 		-	On 1 byte
	Partition's 2nd CHS 	-	On 3 bytes
	Partition's LBA 		-	On 4 bytes
	Part's Sectors number 	-	On 4 bytes

	MBR_END = 2 # On 2 bytes (0x55AA)
"""

from struct import *

class Mbr:

	# The hint used to look the mbr, it is the following sentence "Error loading operating"
	MBR_HINT_PATTERN = b'\x45\x72\x72\x6f\x72\x20\x6c\x6f\x61\x64\x69\x6e\x67\x20\x6f\x70\x65\x72\x61\x74\x69\x6e\x67'

	# The unpack parameters, see above for more information
	BOOT_PRGR_PATTERN = 'c'*442
	DISK_SIGN_PATTERN = 'c'*4
	NULL_PART_PATTERN = 'c'*2
	PART_STATUS_PATTERN = 'c'
	PART_CHS_1_PATTERN = 'c'*3
	PART_TYPE_PATTERN = 'c'
	PART_CHS_2_PATTERN = 'c'*3
	PART_LBA_PATTERN = 'c'*4
	PART_SECT_NB_PATTERN = 'i'


	# The 2 possible status for a partition (there may be others, however, it will mean that the partition is invalid)
	PARTITION_STATUS = {
		'80': 'Bootable Partition',
		'00': 'Non-bootable Partition'
	}

	# All possible partitions types
	PARTITION_TYPES = {
		'00': 'Empty field',
		'01': '12-bit FAT',
		'02': 'XENIX root',
		'03': 'XENIX /usr (obsolete)',
		'04': '16-bit FAT, partition; of sizes less than 32 MB.',
		'05': 'Extended Partition; within the first 1024 cylinders of a disk drive (also see 0F below).',
		'06': '16-bit FAT, partition; greater than or equal to 32 MB.',
		'07': 'Installable file systems: HPFS or NTFS. Also, QNX and Advanced Unix.',
		'08': 'AIX bootable partition, AIX (Linux), SplitDrive, OS/2 (through Version 1.3),',
		'09': 'AIX data partition, AIX bootable (Linux), Coherent file system, QNX.',
		'0A': 'Coherent swap partition, OPUS or OS/2 Boot Manager.',
		'0B': '32-bit FAT',
		'0C': '32-bit FAT, using INT 13 Extensions.',
		'0E': '16-bit FAT >= 32 MB, using INT 13 Extensions.',
		'0F': 'Extended Partition, using INT 13 Extensions; often means it begins past cylinder 1024. ',
		'10': 'OPUS',
		'11': 'Hidden 12-bit FAT.',
		'12': 'Compaq diagnostics.',
		'14': 'Hidden 16-bit FAT, partition <32 MB,',
		'16': 'Hidden 16-bit FAT, partition >= 32 MB',
		'17': 'Hidden IFS (HPFS, NTFS).',
		'18': 'AST Windows swap file',
		'19': 'Willowtech Photon coS',
		'1B': 'Hidden 32-bit FAT',
		'1C': 'Hidden 32-bit FAT, Ext INT 13',
		'1E': 'Hidden 16-bit FAT >32 MB, Ext. INT 13 (PowerQuest specific)',
		'20': 'Willowsoft Overture File System (OFS1)',
		'21': 'reserved (HP Volume Expansion, SpeedStor variant)',
		'22': 'Oxygen Extended',
		'23': 'reserved (HP Volume Expansion, SpeedStor variant?)',
		'24': 'NEC MS-DOS 3.x',
		'26': 'reserved (HP Volume Expansion, SpeedStor variant?)',
		'31': 'reserved (HP Volume Expansion, SpeedStor variant?)',
		'33': 'reserved (HP Volume Expansion, SpeedStor variant?)',
		'34': 'reserved (HP Volume Expansion, SpeedStor variant?)',
		'36': 'reserved (HP Volume Expansion, SpeedStor variant?)',
		'38': 'Theos',
		'3C': 'PowerQuest Files Partition Format',
		'3D': 'Hidden NetWare',
		'40': 'VENIX 80286',
		'41': 'Personal RISC Boot, PowerPC boot partition,',
		'42': 'Secure File System,',
		'43': 'Alternative Linux native file system (EXT2fs)',
		'45': 'Priam, EUMEL/Elan.',
		'46': 'EUMEL/Elan',
		'47': 'EUMEL/Elan',
		'48': 'EUMEL/Elan',
		'4A': 'ALFS/THIN lightweight filesystem for DOS',
		'4D': 'QNX',
		'4E': 'QNX',
		'4F': 'QNX, Oberon boot/data partition.',
		'50': 'Ontrack Disk Manager, read-only partition, FAT partition (Logical sector size varies)',
		'51': 'Ontrack Disk Manager, read/write partition, FAT partition (Logical sector size varies)',
		'52': 'CP/M, Microport System V/386.',
		'53': 'Ontrack Disk Manager, write-only',
		'54': 'Ontrack Disk Manager 6.0 (DDO)',
		'55': 'EZ-Drive 3.05',
		'56': 'Golden Bow VFeature',
		'5C': 'Priam EDISK',
		'61': 'Storage Dimensions SpeedStor',
		'63': 'GNU HURD, Mach, MtXinu BSD 4.2 on Mach, Unix Sys V/386, 386/ix.',
		'64': 'Novell NetWare 286, SpeedStore.',
		'65': 'Novell NetWare (3.11 and 4.1)',
		'66': 'Novell NetWare 386',
		'67': 'Novell NetWare',
		'68': 'Novell NetWare',
		'69': 'Novell NetWare 5+; Novell Storage Services (NSS)',
		'70': 'DiskSecure Multi-Boot',
		'75': 'IBM PC/IX',
		'80': 'Minix v1.1 - 1.4a, Old MINIX (Linux).',
		'81': 'Linux/Minix v1.4b+, Mitac Advanced Disk Manager.',
		'82': 'Linux Swap partition, Prime or Solaris (Unix).',
		'83': 'Linux native file systems (ext2/3/4, JFS, Reiser, xiafs, and others).',
		'84': 'OS/2 hiding type 04h partition;',
		'86': 'NT Stripe Set, Volume Set?',
		'87': 'NT Stripe Set, Volume Set?, HPFS FT mirrored partition.',
		'93': 'Amoeba file system, Hidden Linux EXT2 partition (PowerQuest).',
		'94': 'Amoeba bad block table',
		'99': 'Mylex EISA SCSI',
		'9F': 'BSDI',
		'A0': 'Phoenix NoteBios Power Management "Save to Disk", IBM hibernation.',
		'A1': 'HP Volume Expansion (SpeedStor variant)',
		'A3': 'HP Volume Expansion (SpeedStor variant)',
		'A4': 'HP Volume Expansion (SpeedStor variant)',
		'A5': 'FreeBSD/386',
		'A6': 'OpenBSD',
		'A6': 'HP Volume Expansion (SpeedStor variant)',
		'A7': 'NextStep Partition',
		'A9': 'NetBSD',
		'AA': 'Olivetti DOS with FAT12',
		'B0': 'Bootmanager BootStar by Star-Tools GmbH',
		'B1': 'HP Volume Expansion (SpeedStor variant)',
		'B3': 'HP Volume Expansion (SpeedStor variant)',
		'B4': 'HP Volume Expansion (SpeedStor variant)',
		'B6': 'HP Volume Expansion (SpeedStor variant)',
		'B7': 'BSDI file system or secondarily swap',
		'B8': 'BSDI swap partition or secondarily file system',
		'BB': 'PTS BootWizard (hidden) 4.0; but now also used by',
		'BC': 'May be an Acronis "Backup" or "Secure Zone" partition,',
		'BE': 'Solaris boot partition',
		'C0': 'Novell DOS/OpenDOS/DR-OpenDOS/DR-DOS secured',
		'C1': 'DR-DOS 6.0 LOGIN.EXE-secured 12-bit FAT partition',
		'C2': 'Reserved for DR-DOS 7+',
		'C3': 'Reserved for DR-DOS 7+',
		'C4': 'DR-DOS 6.0 LOGIN.EXE-secured 16-bit FAT partition',
		'C6': 'DR-DOS 6.0 LOGIN.EXE-secured Huge partition',
		'C7': 'Syrinx, Cyrnix, HPFS FT disabled mirrored partition',
		'C8': 'Reserved for DR-DOS 7+',
		'C9': 'Reserved for DR-DOS 7+',
		'CA': 'Reserved for DR-DOS 7+',
		'CB': 'Reserved for DR-DOS secured FAT32',
		'CC': 'Reserved for DR-DOS secured FAT32X (LBA)',
		'CD': 'Reserved for DR-DOS 7+',
		'CE': 'Reserved for DR-DOS secured FAT16X (LBA)',
		'CF': 'Reserved for DR-DOS secured Extended partition (LBA)',
		'D0': 'Multiuser DOS secured (FAT12???)',
		'D1': 'Old Multiuser DOS secured FAT12',
		'D4': 'Old Multiuser DOS secured FAT16 (<= 32M)',
		'D5': 'Old Multiuser DOS secured extended partition',
		'D6': 'Old Multiuser DOS secured FAT16 (BIGDOS > 32 Mb)',
		'D8': 'CP/M 86',
		'DB': 'CP/M, Concurrent CP/M, Concurrent DOS',
		'DE': 'Dell partition. Normally it contains a FAT16 file system of about 32 MB.',
		'DF': 'BootIt EMBRM',
		'E1': 'SpeedStor 12-bit FAT Extended partition, DOS access (Linux).',
		'E2': 'DOS read-only (Florian Painke\'s XFDISK 1.0.4)',
		'E3': 'SpeedStor (Norton, Linux says DOS R/O)',
		'E4': 'SpeedStor 16-bit FAT Extended partition',
		'E5': 'Tandy DOS with logical sectored FAT',
		'E6': 'Storage Dimensions SpeedStor',
		'EB': 'BeOS file system',
		'ED': 'Reserved for Matthias Paul\'s Spryt*x',
		'EE': 'Indicates a GPT Protective MBR followed by a GPT/EFI Header. Used to define a fake partition covering the entire disk.',
		'EF': 'EFI/UEFI System Partition (or ESP); defines a UEFI system partition.' ,
		'F1': 'SpeedStor Dimensions (Norton,Landis)',
		'F2': 'DOS 3.3+ second partition, Unisys DOS with logical sectored FAT.',
		'F3': 'Storage Dimensions SpeedStor',
		'F4': 'SpeedStor Storage Dimensions (Norton,Landis)',
		'F5': 'Prologue',
		'F6': 'Storage Dimensions SpeedStor',
		'FD': 'Reserved for FreeDOS (http://www.freedos.org)',
		'FE': 'LANstep, IBM PS/2 IML (Initial Microcode Load) partition, or...',
		'FF': 'Xenix bad-bloc'}

	def __init__(self, data):
		self.partitions= list()
		self.my_mbr    = data

		self.BOOT_PRGR = unpack(self.BOOT_PRGR_PATTERN, self.my_mbr[:442])
		self.DISK_SIGN = unpack(self.DISK_SIGN_PATTERN, self.my_mbr[442:446])
		self.NULL_PART = unpack(self.NULL_PART_PATTERN, self.my_mbr[446:448])
		
		#Table of partitions

		offset = 0

		#Since each entry of the table has 16 bytes, we had 16 bytes at every round to get the 4 primary partitions' data
		for i in range(4):
			partition = {
				'STATUS' : unpack(self.PART_STATUS_PATTERN, self.my_mbr[448+offset:449+offset]),
				'CHS_1'  : unpack(self.PART_CHS_1_PATTERN,  self.my_mbr[449+offset:452+offset]),
				'TYPE'   : unpack(self.PART_TYPE_PATTERN,   self.my_mbr[452+offset:453+offset]),
				'CHS_2'  : unpack(self.PART_CHS_2_PATTERN,  self.my_mbr[453+offset:456+offset]),
				'LBA'    : unpack(self.PART_LBA_PATTERN,    self.my_mbr[456+offset:460+offset]),
				'SECT_NB': unpack(self.PART_SECT_NB_PATTERN,self.my_mbr[460+offset:464+offset])
			}
			self.partitions.append(partition)
			offset+=16

	# Will make the output more readable
	def to_readable_hex(self, data):
		x = ' '.join( [ "%02X " % ord( x ) for x in data ] ).strip()
		return x

	# Will print the disk's signature only
	def print_disk_signature(self):
		print("\t\t[+] Disk signature:", self.to_readable_hex(self.DISK_SIGN))

	# Will print the selected (or all of them) partition(s)
	def print_mbr_partition(self,num_partition):
		# Just checking the arguments
		if (num_partition > 3) or (num_partition < 0):
			print("\t[!] Invalid argument, wrong number of partitions, 4 Maximum partitions and 1 minimum, leaving ...")
			return

		# Will check if the partition has sectors or not, if not, it will mean that there is no such partition
		if self.partitions[num_partition]['SECT_NB'][0] == 0:
			print("\n\t\t[-] No primary partition", num_partition)
			return

		# Partition's number
		print("\n\t\t[+] Partition ", num_partition+1)
		
		# Partition's status
		the_status = self.to_readable_hex(self.partitions[num_partition]['STATUS'])
		for s in self.PARTITION_STATUS.keys():
			if s in the_status:
				print ("\t\t\tStatus :", self.PARTITION_STATUS[s], '('+the_status+')')

		# Partition's 1st CHS
		print("\t\t\tCHS 1  :", self.to_readable_hex(self.partitions[num_partition]['CHS_1']))

		# Partition's type
		the_type =self.to_readable_hex(self.partitions[num_partition]['TYPE'])
		for t in self.PARTITION_TYPES.keys():
			if t in the_type:
				print ("\t\t\tType   :", self.PARTITION_TYPES[t], '('+the_type+')')

		# Partition's 2nd CHS
		print("\t\t\tCHS 2  :", self.to_readable_hex(self.partitions[num_partition]['CHS_2']))

		# Partition's LBA
		print("\t\t\tLBA    :", self.to_readable_hex(self.partitions[num_partition]['LBA']))

		# Partition's sectors number
		print("\t\t\tNumber of sectors :", self.partitions[num_partition]['SECT_NB'][0])
