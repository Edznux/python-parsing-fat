#!/usr/bin/python3
# coding: utf8

"""
Written by:
	- Ninon MAITRE
	- Edouard SCHWEISGUTH
	- Eric LO
	- Lucas MARCHAL
	- Pierre LIVET
"""

"""
Class BootSector

This class parse and print the boot sector data section.
"""

from struct import *
import datetime

class BootSector:

	# BootSector matching patterns
	BS_HINT_PATTERN1 = b'\xeb\x58\x90'
	BS_END_PATTERN  = b'FAT(32|16|12)*   '

	BS_JMPBOOT_PATTERN     = 'c'*3
	BS_OEMNAME_PATTERN     = 'c'*8
	BPB_BYTSPERSEC_PATTERN = 'h'
	BPB_SECPERCLUS_PATTERN = 'b'
	BPB_RSVDSECCNT_PATTERN = 'h'
	BPB_NUMFATS_PATTERN    = 'b'
	BPB_ROOTENTCNT_PATTERN = 'h'
	BPB_TOTSEC16_PATTERN   = 'h'
	BPB_MEDIA_PATTERN      = 'c'*1
	BPB_FATSZ16_PATTERN    = 'h'
	BPB_SECPERTRK_PATTERN  = 'h'
	BPB_NUMHEADS_PATTERN   = 'h'
	BPB_HIDDSEC_PATTERN    = 'i'
	BPB_TOTSEC32_PATTERN   = 'i'

	BPB_FATSZ32_PATTERN    = 'i'
	BPB_EXTFLAGS_PATTERN   = 'c'*2
	BPB_FSVER_PATTERN      = 'b'*2
	BPB_ROOTCLUS_PATTERN   = 'i'
	BPB_FSINFO_PATTERN     = 'h'
	BPB_BKBOOTSEC_PATTERN  = 'h'
	BPB_RESERVED_PATTERN   = 'c'*12
	BS_DRVNUM_PATTERN      = 'c'*1
	BS_RESERVED1_PATTERN   = 'c'*1
	BS_BOOTSIG_PATTERN     = 'c'*1
	BS_VOLID_PATTERN       = 'c'*4
	BS_VOLLAB_PATTERN      = 'c'*11
	BS_FILSYSTYPE_PATTERN  = 'c'*8

	def __init__(self, boot):
		self.boot = boot

		self.BS_JMPBOOT     = unpack(self.BS_JMPBOOT_PATTERN,     boot[:3])
		self.BS_OEMNAME     = unpack(self.BS_OEMNAME_PATTERN,     boot[3:11])
		self.BPB_BYTSPERSEC = unpack(self.BPB_BYTSPERSEC_PATTERN, boot[11:13])
		self.BPB_SECPERCLUS = unpack(self.BPB_SECPERCLUS_PATTERN, boot[13:14])
		self.BPB_RSVDSECCNT = unpack(self.BPB_RSVDSECCNT_PATTERN, boot[14:16])
		self.BPB_NUMFATS    = unpack(self.BPB_NUMFATS_PATTERN,    boot[16:17])
		self.BPB_ROOTENTCNT = unpack(self.BPB_ROOTENTCNT_PATTERN, boot[17:19])
		self.BPB_TOTSEC16   = unpack(self.BPB_TOTSEC16_PATTERN,   boot[19:21])
		self.BPB_MEDIA      = unpack(self.BPB_MEDIA_PATTERN,      boot[21:22])
		self.BPB_FATSZ16    = unpack(self.BPB_FATSZ16_PATTERN,    boot[22:24])
		self.BPB_SECPERTRK  = unpack(self.BPB_SECPERTRK_PATTERN,  boot[24:26])
		self.BPB_NUMHEADS   = unpack(self.BPB_NUMHEADS_PATTERN,   boot[26:28])
		self.BPB_HIDDSEC    = unpack(self.BPB_HIDDSEC_PATTERN,    boot[28:32])
		self.BPB_TOTSEC32   = unpack(self.BPB_TOTSEC32_PATTERN,   boot[32:36])

		self.BPB_FATSZ32    = unpack(self.BPB_FATSZ32_PATTERN,    boot[36:40])
		self.BPB_EXTFLAGS   = unpack(self.BPB_EXTFLAGS_PATTERN,   boot[40:42])
		self.BPB_FSVER      = unpack(self.BPB_FSVER_PATTERN,      boot[42:44])
		self.BPB_ROOTCLUS   = unpack(self.BPB_ROOTCLUS_PATTERN,   boot[44:48])
		self.BPB_FSINFO     = unpack(self.BPB_FSINFO_PATTERN,     boot[48:50])
		self.BPB_BKBOOTSEC  = unpack(self.BPB_BKBOOTSEC_PATTERN,  boot[50:52])
		self.BPB_RESERVED   = unpack(self.BPB_RESERVED_PATTERN,   boot[52:64])
		self.BS_DRVNUM      = unpack(self.BS_DRVNUM_PATTERN,      boot[64:65])
		self.BS_RESERVED1   = unpack(self.BS_RESERVED1_PATTERN,   boot[65:66])
		self.BS_BOOTSIG     = unpack(self.BS_BOOTSIG_PATTERN,     boot[66:67])
		self.BS_VOLID       = unpack(self.BS_VOLID_PATTERN,       boot[67:71])
		self.BS_VOLLAB      = unpack(self.BS_VOLLAB_PATTERN,      boot[71:82])
		self.BS_FILSYSTYPE  = unpack(self.BS_FILSYSTYPE_PATTERN,  boot[82:90])

		# INFO for BPB_BYTSPERSEC
		self.BPB_BYTSPERSEC_INFO = '(Sector Size (in bytes))'

		# INFO for BPB_SECPERCLUS
		self.BPB_SECPERCLUS_INFO = 'sector(s) per allocation unit'

		# INFO for BPB_RSVDSECCNT
		self.BPB_RSVDSECCNT_INFO = '(Reserved Sectors)'

		# INFO for BPB_NUMFATS
		self.BPB_NUMFATS_INFO = 'FAT(s)'

		# INFO for BPB_MEDIA
		if (self.to_readable_hex(self.BPB_MEDIA) == 'F0'):
			self.BPB_MEDIA_INFO = '(removable media)'
		elif (self.to_readable_hex(self.BPB_MEDIA) == 'F8'):
			self.BPB_MEDIA_INFO = '(non-removable media)'
		else:
			self.BPB_MEDIA_INFO = ''

		# INFO for BPB_SECPERTRK
		self.BPB_SECPERTRK_INFO = '(Sectors per Track)'

		# INFO for BPB_NUMHEADS
		self.BPB_NUMHEADS_INFO = '(Number of heads)'

		# INFO for BPB_HIDDSEC
		self.BPB_HIDDSEC_INFO = '(Number of hidden sectors)'

		# INFO for BPB_TOTSEC32_INFO
		self.BPB_TOTSEC32_INFO = '(Total Number of Sectors)'

		# INFO for BS_DRVNUM
		if (self.to_readable_hex(self.BS_DRVNUM) == '80'):
			self.BS_DRVNUM_INFO = '(hard disk)'
		elif (self.to_readable_hex(self.BS_DRVNUM == '00')):
			self.BS_DRVNUM_INFO = '(floppy disk)'
		else:
			self.BS_DRVNUM_INFO = ''

		# INFO for BS_BOOTSIG
		self.BS_BOOTSIG_INFO = '(Extended Boot Signature)'

		# INFO for BPB_FATSZ32
		self.BPB_FATSZ32_INFO = '(Sectors per FAT)'

		# INFO for BPB_FSVER
		self.BPB_FSVER_INFO = '(File System Version)'

		# INFO for BPB_ROOTCLUS
		self.BPB_ROOTCLUS_INFO = '(First Cluster in the Root Directory)'

		# INFO for BPB_FSINFO
		self.BPB_FSINFO_INFO = '(File System Info Sector)'
		
		# INFO for BPB_BKBOOTSECTOR
		self.BPB_BKBOOTSECTOR_INFO = '(Backup Boot Sector)'

		# INFO for BS_VOLID
		self.BS_VOLID_INFO = '(Volume Serial Number (OS uses date/time to generate this when the volume is formatted))'

		# INFO for BS_VOLLAB
		self.BS_VOLLAB_INFO = '(Volume Label)'

		# INFO for BS_FILSYSTYPE
		self.BS_FILSYSTYPE_INFO = '(File System ID)'

	# convert in hexadecimal (0 prefixed) form
	def to_readable_hex(self, data):
		return ' '.join( [ "%02X " % ord( x ) for x in data ] ).strip()
		

	# convert in a better text version (stripped down)
	def to_readable_string(self, data):
		x = b''.join(data).strip()
		return x.decode('ascii')

	# convert into human readable date format
	def to_readable_date(self, data):
		x = ''.join(["%02X"%ord(x) for x in data]).strip()
		date = datetime.datetime.fromtimestamp(float(int('0x'+x, 16)))
		return x + ' ('+ str(date) + ')'

	# convert bytes (BPB_EXTFLAGS) into FAT table informations
	def to_readable_BPB_EXTFLAGS(self,data):

		data_little = (int.from_bytes(data[0], byteorder='little'))

		nb_activ_fat = (data_little & 0b11110000) >> 4

		if ((data_little & 0b00000001) == 0):
			return ('FAT is mirrored at runtime into all FATs')
		elif((data_little & 0b00000001) == 1):
			return (str(nb_activ_fat+1) + ' active FAT')
		else:
			return ('Error')

	# convert and "verify" the file system version
	def to_readable_BPB_FSVER(self, data):
		if (data[0] == 0 and data[1] == 0):
			return (str(data[0])+':'+str(data[1]))
		else:
			return ('Error')

	def print_BPB(self):
		print("\tBoot sector's primary part")
		print('\t\t\tBS_JMPBOOT     :', self.to_readable_hex(self.BS_JMPBOOT))
		print('\t\t\tBS_OEMNAME     :', self.to_readable_string(self.BS_OEMNAME))
		print('\t\t\tBPB_BYTSPERSEC :', self.BPB_BYTSPERSEC[0], self.BPB_BYTSPERSEC_INFO)
		print('\t\t\tBPB_SECPERCLUS :', self.BPB_SECPERCLUS[0], self.BPB_SECPERCLUS_INFO)
		print('\t\t\tBPB_RSVDSECCNT :', self.BPB_RSVDSECCNT[0], self.BPB_RSVDSECCNT_INFO)
		print('\t\t\tBPB_NUMFATS    :', self.BPB_NUMFATS[0], self.BPB_NUMFATS_INFO)
		print('\t\t\tBPB_ROOTENTCNT :', self.BPB_ROOTENTCNT[0])
		print('\t\t\tBPB_TOTSEC16   :', self.BPB_TOTSEC16[0])
		print('\t\t\tBPB_MEDIA      :', self.to_readable_hex(self.BPB_MEDIA), self.BPB_MEDIA_INFO)
		print('\t\t\tBPB_FATSZ16    :', self.BPB_FATSZ16[0])
		print('\t\t\tBPB_SECPERTRK  :', self.BPB_SECPERTRK[0], self.BPB_SECPERTRK_INFO)
		print('\t\t\tBPB_NUMHEADS   :', self.BPB_NUMHEADS[0], self.BPB_NUMHEADS_INFO)
		print('\t\t\tBPB_HIDDSEC    :', self.BPB_HIDDSEC[0], self.BPB_HIDDSEC_INFO)
		print('\t\t\tBPB_TOTSEC32   :', self.BPB_TOTSEC32[0], self.BPB_TOTSEC32_INFO)
		print('\n')

	def print_FAT16(self):
		print('\tFAT12/16 Structure : ')
		print('\t\t\tBS_DRVNUM      :', self.to_readable_hex(self.BS_DRVNUM), self.BS_DRVNUM_INFO)
		print('\t\t\tBS_RESERVED1   :', self.to_readable_hex(self.BS_RESERVED1))
		print('\t\t\tBS_BOOTSIG     :', self.to_readable_hex(self.BS_BOOTSIG), self.BS_BOOTSIG_INFO)
		print('\t\t\tBS_VOLID       :', self.to_readable_date(self.BS_VOLID), self.BS_VOLID_INFO)
		print('\t\t\tBS_VOLLAB      :', self.to_readable_string(self.BS_VOLLAB), self.BS_VOLLAB_INFO)
		print('\t\t\tBS_FILSYSTYPE  :', self.to_readable_string(self.BS_FILSYSTYPE), self.BS_FILSYSTYPE_INFO)
		print('\n')

	def print_FAT32(self):
		print("\tFAT32 Structure : ")
		print('\t\t\tBPB_FATSZ32    :', self.BPB_FATSZ32[0], self.BPB_FATSZ32_INFO)
		print('\t\t\tBPB_EXTFLAGS   :', self.to_readable_BPB_EXTFLAGS(self.BPB_EXTFLAGS))
		print('\t\t\tBPB_FSVER      :', self.to_readable_BPB_FSVER(self.BPB_FSVER), self.BPB_FSVER_INFO)
		print('\t\t\tBPB_ROOTCLUS   :', self.BPB_ROOTCLUS[0], self.BPB_ROOTCLUS_INFO)
		print('\t\t\tBPB_FSINFO     :', self.BPB_FSINFO[0], self.BPB_FSINFO_INFO)
		print('\t\t\tBPB_BKBOOTSEC  :', self.BPB_BKBOOTSEC[0], self.BPB_BKBOOTSECTOR_INFO)
		print('\t\t\tBPB_RESERVED   :', self.to_readable_hex(self.BPB_RESERVED))
		print('\t\t\tBS_DRVNUM      :', self.to_readable_hex(self.BS_DRVNUM), self.BS_DRVNUM_INFO)
		print('\t\t\tBS_RESERVED1   :', self.to_readable_hex(self.BS_RESERVED1))
		print('\t\t\tBS_BOOTSIG     :', self.to_readable_hex(self.BS_BOOTSIG), self.BS_BOOTSIG_INFO)
		print('\t\t\tBS_VOLID       :', self.to_readable_date(self.BS_VOLID), self.BS_VOLID_INFO)
		print('\t\t\tBS_VOLLAB      :', self.to_readable_string(self.BS_VOLLAB), self.BS_VOLLAB_INFO)
		print('\t\t\tBS_FILSYSTYPE  :', self.to_readable_string(self.BS_FILSYSTYPE), self.BS_FILSYSTYPE_INFO)
		print('\n')

	def print_info(self):
		self.print_BPB()
		if (self.BPB_TOTSEC16[0] != 0 and self.BPB_TOTSEC32[0] == 0):
			self.print_FAT16()
		elif (self.BPB_TOTSEC16[0] == 0 and self.BPB_TOTSEC32[0] != 0):
			self.print_FAT32()
		else:
			print ('\t[-] Error to determine the type of FAT\n')

