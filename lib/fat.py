#!/usr/bin/python3
# coding: utf8


"""

Written by:

    - Ninon MAITRE

    - Edouard SCHWEISGUTH

    - Eric LO

    - Lucas MARCHAL

    - Pierre LIVET

"""
"""
Written by:
	- Ninon MAITRE
	- Edouard SCHWEISGUTH
	- Eric LO
	- Lucas MARCHAL
	- Pierre LIVET
"""


"""
Classe FAT
"""
from struct import *

class Fat:


	FAT_HINT_PATTERN = b'\xf8\xff\xff\x0f\xff\xff\xff\xff'

	def __init__(self, boot):
		print("\t[+] FAT class Description")

	def print_info(Parser,liste):
		for off7 in liste :
			print("\n\t[+] FAT number :\t",liste.index(off7)+1)
			allocated_list = list()
			allocated_listbis = list()
			arbitrary_nb = 5750
			for i in range(arbitrary_nb):				
				data = Parser.data[off7+(i*4):off7+((i+1)*4)]
				hint1 = b'\xff\xff\xff\x0f'
				hint2 = b'\xff\xff\xff\xff'
				hint3 = b'\xf8\xff\xff'	
				hintbis = b'\x00\x00\x00\x00'
				if hint1 in data or hint2 in data or hint3 in data :
					allocated_list.append(i)
				if hintbis in data :
					allocated_listbis.append(i)

			print("\t[-] List of allocated clusters:\n")
			cpt = 0.1
			print("[\t",end="")
			for element in allocated_list :
				if cpt < 9 :
					if cpt == 0:
						print("\t",end="")
					print(element,"\t",end="")
					cpt = cpt+1
				else : 
					print(element)
					cpt = 0
			print("]\n")
		

			print("\t[-] List of free clusters (number of free clusters = ", len(allocated_listbis),"):\n")
			cpt = 0.1
			print("[\t",end="")
			for element in allocated_listbis :
				if element > 200 :
					print("...",end="")
					break				
				if cpt < 9 :
					if cpt == 0:
						print("\t",end="")
					print(element,"\t",end="")
					cpt = cpt+1
				else : 
					print(element)
					cpt = 0
			print("]")

