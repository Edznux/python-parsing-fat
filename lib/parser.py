"""
Written by:
	- Ninon MAITRE
	- Edouard SCHWEISGUTH
	- Eric LO
	- Lucas MARCHAL
	- Pierre LIVET
"""

import re

from struct import *
from .mbr import Mbr
from .bs import BootSector
from .fat import Fat

class Parser:
	def __init__(self, data):
		self.data = data

	def getBPB(self):
		print("[*] Getting boot sector info\n")
		liste_end = self.searchPattern(BootSector.BS_END_PATTERN)
		not_found = True
		liste_bs = list()

		while (not_found == True):
			for offset in liste_end:
				if offset>90:
					boot_s = self.data[offset-82:offset+8]
					if (BootSector.BS_HINT_PATTERN1) in boot_s:
						not_found = False
						liste_bs.append(boot_s)
			break

		if (len(liste_bs)) == 1:
			print ("[+] {} boot Sector Found !\n".format(len(liste_bs)))
		else:
			print ("[+] {} boot Sectors Found !\n".format(len(liste_bs)))
		
		for i in (range(len(liste_bs))):
			print("Boot sector number ", i+1, " :")
			bs = BootSector(liste_bs[i])
			bs.print_info()

	def getFAT(self):
		print("[*] Getting allocation table")
		liste = self.searchPattern(Fat.FAT_HINT_PATTERN)
		if not liste : 
			print("\t[+] FAT not found !")			
		else:	
			print("\t[+] FAT's portions found !")
			
			""" Previous version			
			for offset in liste :
				liste_fat.append(self.data[offset:offset+5632])
			fat = Fat(liste_fat)
			"""
			print("\t[+] FAT Found !")	
			
			Fat.print_info(self, liste)			

	def getRootFiles(self):

		print("[*] Getting root files")

	def getMBR(self):

		print("\t[*] Searching MBR pattern in the dump...")
		liste_end=self.searchPattern(b'\x55\xAA')
		
		not_found = True
		while (not_found == True):
			for offset in liste_end:
				if offset>512:
					my_mbr = self.data[offset-512:offset]
					if (Mbr.MBR_HINT_PATTERN) in my_mbr:
						not_found = False
						print ("\t[+] MBR Found !")
						break
		
		m = Mbr(my_mbr)
		print("\t[*] Parsing MBR\n")
		m.print_disk_signature()
		for i in range(4):
			m.print_mbr_partition(i)

	def parse(self):
		print("\n================= MBR =================\n")
		self.getMBR()
		print("\n============= Boot Sector =============\n")
		self.getBPB()
		print("\n======== File allocation table ========\n")
		self.getFAT()
		#print("\n============== Root Files =============\n")
		#self.getRootFiles()


	def searchPattern(self, pattern, data=""):
		res = list()

		if data == "":
			data = self.data
		
		for found in re.finditer(pattern, data):
			res.append(found.start())

		return res
