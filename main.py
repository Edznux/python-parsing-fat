#!/usr/bin/python3

"""
Written by:
	- Ninon MAITRE
	- Edouard SCHWEISGUTH
	- Eric LO
	- Lucas MARCHAL
	- Pierre LIVET
"""

import sys, os
from lib.parser import Parser

# Print the usage section
def usage():
	print("{} <dump file.dmp>".format(sys.argv[0]))

def main():
	
	filename=sys.argv[1]

	print ("\nCurrent file : ", filename, "\n")	

	try:
		dump = open(filename, "rb").read()
	except:
		print("Cannot open file {}".format(filename))
		sys.exit(1)

	# load the parser and start the parsing.
	f = Parser(dump)
	f.parse()

if __name__ == "__main__":

	# Check if the script as mandatory arguments
	if(len(sys.argv) != 2):
		usage()
		sys.exit(1)

	print ("\n#### Memory dump parser ####\n")

	# On laisse ça ou pas ?
	print("\n Written by : ")
	print("\t - Ninon MAITRE")
	print("\t - Edouard SCHWEISGUTH")
	print("\t - Eric LO")
	print("\t - Lucas MARCHAL")
	print("\t - Pierre LIVET")

	main()
